# DNS_CHECK
Spits a DNS function into ram, greps out any garbage info. Planning to clean up better

dns()

{

clear

traceroute $1&

echo "NSLookup for $1"

nslookup $1

echo "Dig for $1"

echo "A Records"

dig $1 any +short

echo "SOA Records"

dig soa $1 +short

echo "Nameservers"

dig ns $1 +short

echo "Whois for $1"

whois $1.com | grep Domain

echo "Whois got our Nameservers?" whois $1.com | grep NS

echo "Expired?"

whois $1 | grep Expiry

echo "Can we ping it?" ping -c 4 $1

curl -A "Firefox" $1

whois $(dig $1 +short | head -n1) | grep OrgName

}
